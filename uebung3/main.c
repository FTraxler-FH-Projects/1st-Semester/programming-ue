#include "stdio.h"
#include "stdlib.h"
#include "time.h"

void task0();
void task1(int students);
float average(int *array, int students);
int countNegativeGrades(int *array, int students);
void task2();
void task3();
int random(int max);

int main()
{
    //printf("Task 0 (introduction)\n");
    //task0();
    printf("Task 1\n");
    task1(10);
    printf("Task 2\n");
    task2();
    printf("Task 3 (optional)\n");
    task3();
    //
    printf("\npress any key to continue...");
    getchar();
    return 0;
}

void task0()
{
    int input;
    int reversed = 0;
    //
    printf("please enter any number. I will reverse it: ");
    while (1)
    {
        if (scanf("%i", &input))
            break;
        else
            printf("invalid input! try again\n");
    }
    //
    while (input > 0)
    {
        reversed = (reversed * 10) + (input % 10);
        input /= 10;
    }
    //
    printf("%i", reversed);
    getchar();
}

void task1(int students)
{
    printf("Please enter the grades of all 10 students.\n");
    int grades[students];
    for (int i = 0; i < 10; i++)
    {
        while (1)
        {
            if (scanf("%i", &grades[i]) != 0 && grades[i] > 0 && grades[i] < 6)
                break;
            else
                printf("the input is not valid! please try again\n");
        }
    }
    printf("There are %i negative grades in class.\n", countNegativeGrades(grades, students));
    printf("The average is: %f\n", average(grades, students));
    getchar();
}

float average(int *array, int students)
{
    int count = 0;
    for (int i = 0; i < students; i++)
    {
        count += *(array + i);
    }
    return (float)count / (float)students;
}

int countNegativeGrades(int *array, int students)
{
    int neg = 0;
    for (int i = 0; i < students; i++)
    {
        if (*(array + i) == 5)
            neg++;
    }
    return neg;
}

void task2()
{
    printf("> now guess a number between 1 and 100 within 15 guesses.\n");
    int rnd = random(100);
    int input;
    for (int i = 1; i != 15; i++)
    {
        while (1)
        {
            printf("(%i): ", i);
            if (scanf("%i", &input))
                break;
            else
                printf(">> the input is not valid! please try again\n");
        }
        if (input == rnd)
        {
            printf("> thats the right number. You needed %i guesses.\n", i);
            break;
        }
        else if (input > rnd)
            printf("> your number is too big. try again.\n");
        else if (input < rnd)
            printf("> your number is too small. try again.\n");
    }
    getchar();
}

void task3()
{
    printf("> now guess a character between a and z within 10 guesses.\nplease only use lower caps characters\n");
    char rnd = 'a' + random(25);
    //printf("%c\n", rnd);
    char input;
    for (int i = 1; i != 10; i++)
    {
        while (1)
        {
            printf("(%i): ", i);
            if ((scanf(" %c", &input)) && (input > 'a' && input < 'z'))
                break;
            else
                printf(">> the input is not valid! please try again\n");
        }
        if (input == rnd)
        {
            printf("> thats the right character. You needed %i guesses.\n", i);
            break;
        }
        else if (input > rnd)
            printf("> your character is too near at the end of the alphabet. try again.\n");
        else if (input < rnd)
            printf("> your character is too near the beginning of the alphabet. try again.\n");
    }
    getchar();
}

int random(int max)
{
    srand(time(NULL)); // init
    rand();            // remove first number
    return 1 + (int)(max * rand() / (RAND_MAX + 1.0));
}