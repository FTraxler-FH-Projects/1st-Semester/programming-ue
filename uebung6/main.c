#include <stdio.h>

char *stringString(char *haystack, char *needle)
{
    while (haystack != '\0')
    {
        if (*haystack == *needle)
        {
            int flag = 0;
            for (int i = 0; *(needle + i) != '\0'; i++)
            {
                if (*(haystack + i) != *(needle + i))
                {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0)
                return haystack;
        }
        haystack++;
    }
    return '\0';
}

int main()
{
    char haystack[100] = "Dies ist ein dummy Text";
    char needle[100] = "ein";

    printf("%s", stringString(haystack, needle));
    getchar();

    return 0;
}