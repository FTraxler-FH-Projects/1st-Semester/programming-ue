/*
 * Author: FTraxler (florian.traxler@stud.fh-campuswien.ac.at) @2017-12-14 
 * Compiled with MinGW; C99 standard! 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(float *a, float *b);
void memswap(char *mem1, char *mem2, int size);
int firstPosOfChar(char *text, char c);
void capitalize(char *text);

void swap(float *a, float *b)
{
    float temp = *b;
    *b = *a;
    *a = temp;
}

void memswap(char *mem1, char *mem2, int size)
{
    char temp[size];
    for (int i = 0; i < size; i++)
    {
        *(temp + i) = *(mem2 + i);
        *(mem2 + i) = *(mem1 + i);
        *(mem1 + i) = *(temp + i);
    }
}

int firstPosOfChar(char *text, char c)
{
    int counter = 0;
    while (*text != '\0')
    {
        if (*text == c)
            break;
        text++;
        counter++;
    }
    return counter;
}

void capitalize(char *text)
{
    if ((int)*text >= 97)
        *text -= 32;
    text++;
    while (*text != '\0')
    {
        if ((*(text - 1) == ' ') && ((int)*text >= 97))
            *text -= 32;
        text++;
    }
}

int main()
{
    float num1, num2;
    int i;
    int data1[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int data2[10] = {101, 234, 398, 434, 523, 616, 748, 856, 901, 133};
    char text[100];
    //
    num1 = 10.56f;
    num2 = 1.12E3f;
    //
    printf("\n======================================\n\n");
    printf("numbers before swap: num1 = %.2f, num2 = %.2f\n", num1, num2);
    swap(&num1, &num2);
    printf("numbers after swap: num1 = %.2f, num2 = %.2f\n", num1, num2);
    //
    printf("\n======================================\n\n");
    printf("array before memswap:\n");
    for (i = 0; i < 10; i++)
    {
        printf("%d\t\t%d\n", data1[i], data2[i]);
    }
    memswap((char *)data1, (char *)data2, sizeof(data1));
    printf("array after memswap:\n");
    for (i = 0; i < 10; i++)
    {
        printf("%d\t\t%d\n", data1[i], data2[i]);
    }
    //
    printf("\n======================================\n\n");
    strcpy(text, "Dies ist ein Beispieltext, wo jedes Wort mit einem Grossbuchstaben beginnen soll.");
    printf("text to examine: %s\n", text);
    //
    i = firstPosOfChar(text, 'j');
    printf("first position of charachter '%c': %d (should be 30)\n", 'j', i);
    i = firstPosOfChar(text, 'G');
    printf("first position of charachter '%c': %d (should be 51)\n", 'G', i);
    //
    printf("\n======================================\n\n");
    strcpy(text, "Dies ist ein Beispieltext, wo jedes Wort mit einem Grossbuchstaben beginnen soll.");
    printf("text before capitalize: %s\n", text);
    capitalize(text);
    printf("text after capitalize: %s\n", text);
    printf("\n======================================\n\n");
    //
    getchar();

    return 0;
}
