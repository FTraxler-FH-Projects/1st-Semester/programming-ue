#include <stdio.h>

char board[9] = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
int player = 1;
int gameover = 0;

void printBoard();                      // done
void printLine();                       // done
void resetBoard();                      // done
int getField(int fieldNo);              // done
void setField(int player, int fieldNo); // done
void checkWinner();                     // done
void togglePlayer();                    // done

int main()
{
    // startup
    printf("Welcome to TicTacToe!\n");
    printf("Here you can find an overview on the field numbers.\n ");
    printBoard();
    // start game
    resetBoard();
    while (gameover == 0)
    {
        printf("It's your turn Player %i.\n", player);
        printBoard();
        printf("Please enter your field number: ");
        int chosenField = 0;
        while (scanf("%i", &chosenField) == 0)
        {
            printf("This is not a valid input. please try again:");
            getchar();
        }
        if (getField(chosenField) == ' ')
        {
            setField(player, chosenField);
            checkWinner();
            togglePlayer();
        }
    }
    // keep console alive
    printf("press any key to continue...\n");
    getchar();
    return 0;
}

// change player
void togglePlayer() // (c) by Kerstin
{
    if (player == 1)
    {
        player++;
    }
    else if (player == 2)
    {
        player--;
    }
}

void resetBoard() // (c) by Kerstin
{
    for (int i = 0; i < 9; i++)
    {
        board[i] = ' ';
    }
    gameover = 0;
}

void setField(int player, int fieldNo) // (c) by Kerstin
{
    if (player == 1)
    {
        board[fieldNo - 1] = 'x';
    }
    else if (player == 2)
    {
        board[fieldNo - 1] = 'o';
    }
}

int getField(int fieldNo) // (c) by Kerstin
{
    return board[fieldNo - 1];
}

void checkWinner()
{
    if (
        (board[0] == board[1] && board[1] == board[2] && board[0] != ' ') ||
        (board[3] == board[4] && board[4] == board[5] && board[3] != ' ') ||
        (board[6] == board[7] && board[7] == board[8] && board[6] != ' ') ||
        (board[0] == board[3] && board[3] == board[6] && board[0] != ' ') ||
        (board[1] == board[4] && board[4] == board[7] && board[1] != ' ') ||
        (board[2] == board[5] && board[5] == board[8] && board[2] != ' ') ||
        (board[0] == board[4] && board[4] == board[8] && board[0] != ' ') ||
        (board[6] == board[4] && board[4] == board[2] && board[6] != ' '))
    {
        printf("You won Player %i \n", player);
        gameover = 1;
    }
}

void printBoard() // (c) by Kerstin
{
    printf("\n");
    printf(" %c | %c | %c \n", board[0], board[1], board[2]);
    printLine();
    printf(" %c | %c | %c \n", board[3], board[4], board[5]);
    printLine();
    printf(" %c | %c | %c \n", board[6], board[7], board[8]);
    printf("\n");
}

void printLine() // (c) by Kerstin
{
    for (int i = 0; i < 11; i++)
    {
        printf("%c", 196);
    }
    printf("\n");
}