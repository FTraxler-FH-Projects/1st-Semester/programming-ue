/*
 * Author: FTraxler (florian.traxler@stud.fh-campuswien.ac.at) @2017-11-14 16:27:41 
 * Compiled with MinGW; C99 standard! 
 */ 
 
#include "stdio.h"

void task1()
{
    int output = 1;
    for(int row = 1; row < 5; row++)
    {
        for(int col = 1; col <= row; col ++)
        {
            printf("%i ",output);
            output ++;
        }
        printf("\n");
    }
}

int toggle(int in)
{
    if(in == 1)
        return 0;
    else
        return 1;
}

void task2()
{
    int i = 0;
    for(int row = 1; row < 6; row++)
    {
        i = toggle(i);
        for(int col = 1; col <= row; col++)
        {
            if(col % 2 == 0)
                printf("%i ",i);
            else
                printf("%i ", toggle(i));
        }
        printf("\n");
    }
}

void sieve(int lower, int upper)
{
    int count = upper - lower;
    int field[count];

    for(int i = 0; i < count; i++)
    {
        field[i] = lower;
        lower++;
    }

    // i know, it could be done better, but this was the easiest way :-)
    for(int i = 0; i < count; i++)
    {  
        if(field[i]%2 == 0 && field[i] != 2)
            field[i] = 0;
        else if(field[i]%3 == 0 && field[i] != 3)
            field[i] = 0;
        else if(field[i]%5 == 0 && field[i] != 5)
            field[i] = 0;
        else if(field[i]%7 == 0 && field[i] != 7)
            field[i] = 0;
    }

    for(int i=1; i < count; i++)
    {
        if(field[i])
            printf("%i, ", field[i]);
    }
}

void task3()
{
    printf("sieve of erathosthenes\n");
    printf("please enter the start number: ");
    int lower = 0;
    scanf("%i", &lower);
    printf("please enter upper number: ");
    int upper = 0;
    scanf("%i", &upper);
    getchar();
    sieve(lower, upper);
    printf("\n");
}

int main()
{
    printf("Task 1:\n");
    task1();
    printf("Task 2:\n");
    task2();
    printf("Task 3:\n");
    task3();
    //
    printf("Press any key to continue...");
    getchar();
    return 0;
}