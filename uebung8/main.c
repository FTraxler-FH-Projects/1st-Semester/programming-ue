#include "image.h"

int main(int argc, const char **argv)
{
	struct header bmpHeader;
	struct info bmpInfo;
	struct pixel *data;
	struct pixel *mirrorData;
	struct pixel *inverseData;

	data = readBitmap("input.bmp", &bmpHeader, &bmpInfo);
	mirrorData = mirror(&bmpInfo, data);
	inverseData = invert(&bmpInfo, data);
	writeBitmap("mirrored.bmp", &bmpHeader, &bmpInfo, mirrorData);
	writeBitmap("inverted.bmp", &bmpHeader, &bmpInfo, inverseData);

	// TODO GEBEN SIE NICHT MEHR BENÖTIGTE SPEICHERBEREICHE SPÄTESTENS AN DIESER STELLE WIEDER FREI.
	free(data);
	free(mirrorData);
	free(inverseData);

	return 0;
}
