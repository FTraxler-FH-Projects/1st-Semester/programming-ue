/*
 *  image.c
 */

#include "image.h"

struct pixel *readBitmap(const char *filename, struct header *pHeader, struct info *pInfo)
{
	struct pixel *pData;
	FILE *fp;

	fp = fopen(filename, "rb");
	if (fp == NULL)
	{
		printf("Datei %s kann nicht geoeffnet werden!\n", filename);
		return NULL;
	}

	fread(&pHeader->bfType, sizeof(char), 2, fp);
	fread(&pHeader->bfSize, sizeof(unsigned int), 1, fp);
	fread(&pHeader->bfReserved, sizeof(unsigned int), 1, fp);
	fread(&pHeader->bfOffBits, sizeof(unsigned int), 1, fp);
	fread(pInfo, sizeof(struct info), 1, fp);

	// TODO: IMPLEMENTIEREN SIE AN DIESER STELLE DAS EINLESEN DER BILDDATEN (PIXEL)
	pData = (struct pixel *)malloc(pInfo->biWidth * pInfo->biHeight * sizeof(struct pixel));
	fread(pData, sizeof(struct pixel), pInfo->biWidth * pInfo->biHeight, fp);

	fclose(fp);

	return pData;
}

void writeBitmap(const char *filename, struct header *pHeader, struct info *pInfo, struct pixel *pData)
{
	FILE *fp;

	fp = fopen(filename, "wb");
	if (fp == NULL)
	{
		printf("Datei %s kann nicht geoeffnet werden!\n", filename);
		return;
	}

	// TODO IMPLEMENTIEREN SIE AN DIESER STELLE DAS SPEICHERN DES BITMAPS
	fwrite(&pHeader->bfType, sizeof(char), 2, fp);
	fwrite(&pHeader->bfSize, sizeof(unsigned int), 1, fp);
	fwrite(&pHeader->bfReserved, sizeof(unsigned int), 1, fp);
	fwrite(&pHeader->bfOffBits, sizeof(unsigned int), 1, fp);
	fwrite(pInfo, sizeof(struct info), 1, fp);

	fwrite(pData, sizeof(struct pixel), pInfo->biWidth * pInfo->biHeight, fp);

	fclose(fp);
}

struct pixel *mirror(struct info *pInfo, struct pixel *pData)
{
	/*
	 * TODO IMPLEMENTIEREN SIE AN DIESER STELLE DAS HORIZONTALE UND VERTIKALE SPIEGELN
	 * ERSETZEN SIE "RETURN NULL" DURCH EIN SINNVOLLES RETURN STATEMENT
	 */
	int maxPixel = pInfo->biWidth * pInfo->biHeight - 1;
	struct pixel *pNew;
	pNew = (struct pixel *)malloc(pInfo->biWidth * pInfo->biHeight * sizeof(struct pixel));
	for (int i = maxPixel; i >= 0; i--)
	{
		*pNew = *(pData + i);
		pNew++;
	}
	return pNew - maxPixel;
}

struct pixel *invert(struct info *pInfo, struct pixel *pData)
{
	/*
	 * TODO IMPLEMENTIEREN SIE AN DIESER STELLE DAS HORIZONTALE UND VERTIKALE SPIEGELN
	 * ERSETZEN SIE "RETURN NULL" DURCH EIN SINNVOLLES RETURN STATEMENT
	 */
	int maxPixel = pInfo->biWidth * pInfo->biHeight - 1;
	struct pixel *pNew;
	pNew = (struct pixel *)malloc(pInfo->biWidth * pInfo->biHeight * sizeof(struct pixel));
	for (int i = 0; i <= maxPixel; i++)
	{
		pNew->blue = ~(pData + i)->blue;
		pNew->green = ~(pData + i)->green;
		pNew->red = ~(pData + i)->red;
		pNew++;
	}
	return pNew - maxPixel;
}

struct pixel *saturation(struct info *pInfo, struct pixel *pData, double wRed, double wGreen, double wBlue)
{
	/*
	 * TODO IMPLEMENTIEREN SIE AN DIESER STELLE DAS ÄNERDN DER FARBSÄTTIGUNG
	 * ERSETZEN SIE "RETURN NULL" DURCH EIN SINNVOLLES RETURN STATEMENT
	 */
	return NULL;
}
