## Important notice  
  
The code in this repository is written using *Visual Studio Code* as an editor and *MinGW* as compiler.  
There may occur problems when using other compilers.  
Common changes to be applied can be the `#include <...>` instead of `#include "..."`.
Also the `#define _CRT_SECURE_NO_WARNINGS` flag can be missing.  
  
Please be aware, that the code here is just for help and learning purposes.  
**Using ths code exactly as is in your own hand in or homework is strictly forbidden**  
  
Feel free to contribute your code after taking a look at the *CONTRIBUTING.md* file.