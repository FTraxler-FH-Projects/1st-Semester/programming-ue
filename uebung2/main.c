/*
 * Author: FTraxler (florian.traxler@stud.fh-campuswien.ac.at) @2017-11-14 18:47:22 
 * Compiled with MinGW; C99 standard! 
 */ 
 
#include "stdio.h"
#include "stdlib.h"

// function declarations
void task1();
void task2();
void task3();
void task4();
void task5();
void task6();
void task7();

// main
int main()
{
    printf("Task 1:\n");
    task1();
    printf("\nTask 2:\n");
    task2();
    printf("\nTask 3:\n");
    task3();
    printf("\nTask 4:\n");
    task4();
    // break
    printf("\npress any key to continue...");
    getchar();
    printf("\nTask 5:\n");
    task5();
    printf("\nTask 6:\n");
    task6();
    printf("\nTask 7:\n");
    task7();
    // break
    printf("\n\npress any key to continue...");
    getchar();
    return 0;
}

void task1()
{
    int field[3];
    printf("please enter three integer numbers. I will give you the average value\n");
    for (int i = 0; i < 3; i++)
    {
        if (scanf("%i", &field[i]) == 0)
        {
            printf("An error occured parsing your input.\n");
            getchar();
            goto end;
        }
    }
    printf("The average is: %i\n", (field[0] + field[1] + field[2]) / 3);
end:
    getchar();
}

void task2()
{
    float field[3];
    printf("please enter length, width and depth in meters. I will give you the volume.\n");
    for (int i = 0; i < 3; i++)
    {
        if (scanf("%f", &field[i]) == 0)
        {
            printf("An error occured parsing your input.\n");
            getchar();
            goto end;
        }
    }
    printf("The volume is: %f qm\n", field[0] * field[1] * field[2]);
    printf("that's %f l.\n", (field[0] * field[1] * field[2]) * 1000);
end:
    getchar();
}

void task3()
{
    printf("Please enter two numbers. I will give you the smaller.\n");
    float field[2];
    for (int i = 0; i < 2; i++)
    {
        if (scanf("%f", &field[i]) == 0)
        {
            printf("An error occured parsing your input.\n");
            getchar();
            goto end;
        }
    }
    if (field[0] > field[1])
        printf("%f is the smallest number\n", field[1]);
    else
        printf("%f is the smallest number\n", field[0]);
end:
    getchar();
}

void task4()
{
    printf("please enter a year: ");
    int year;
    if (scanf("%i", &year) == 0)
    {
        printf("An error occured parsing your input.\n");
        getchar();
        goto end;
    }
    if ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0))
        printf("%i is a leap-year\n", year);
    else
        printf("%i is not a leap-year\n", year);
end:
    getchar();
}

void task5()
{
    for (int i = 7; i != 0; i--)
    {
        for (int ii = 1; ii <= i; ii++)
        {
            printf("*");
        }
        printf("\n");
    }
}

void task6()
{
    for (int i = -1; i < 5; i++)
    {
        for (int ii = 1; ii <= 11; ii++)
        {
            if (ii < 5 - i || ii > 7 + i)
                printf(" ");
            else
                printf("*");
        }
        printf("\n");
    }
}

void task7()
{
    for (int i = 0; i <= 1000; i++)
    {
        if (i % 7 == 0)
            printf("%i, ", i);
    }
}