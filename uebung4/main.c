#include "stdio.h"
#include "math.h"

// task one combines both variants and compares them.
void task1();
void bisection(float x1, float x2, float epsilon);
float avg(float x1, float x2);
float function(float x);
void newton(float x1, float epsilon);

int main()
{
    printf("Task 1:\n");
    task1();
    getchar();
    //
    printf("\npress any key to continue...");
    getchar();
    return 0;
}

void task1()
{
    // get data
    float input[3];
    printf("please enter the two function values\n");
    if (scanf("%f", &input[0]) == 0)
        goto catch;
    if (scanf("%f", &input[1]) == 0)
        goto catch;
    printf("please enter an epsilon\n");
    if (scanf("%f", &input[2]) == 0)
        goto catch;
    // calculation
    printf("calculating...\n");
    // printf("f(x)=%f\n", function(input[0])); // debug
    // printf("f(x)=%f\n", function(input[1])); // debug
    //bisection(input[0], input[1], input[2]);
    newton(input[0], input[2]);
    return;
    // error handling
    catch : printf("error when parsing input. program aborted.\n");
    return;
}

float function(float x)
{
    return (2 * x + 3);
}

float avg(float x1, float x2)
{
    return (x1 + x2) / 2;
}

void bisection(float x1, float x2, float epsilon)
{
    float x = avg(x1, x2);
    printf("x=%f\n", x); // debug
    if (fabs(x1 - x2) > epsilon)
    {
        if (function(x1) < 0 && function(x) > 0)
            bisection(x1, x, epsilon);
        else if (function(x) < 0 && function(x2) > 0)
            bisection(x, x2, epsilon);
    }
    else
        printf("the zero point lies at f(%f)=%f with a deviation of %f.\n", x, function(x), epsilon);
    // because if the zero point is found exactly the if/else structure will not get it -> don't even dare ask me why, there is another check -.-
    if (function(x) == 0)
        printf("the zero point lies exactly at f(%f)=0.\n", x);
}

void newton(float x1, float epsilon)
{
    float x = x1 - (function(x1) / (2*x1));
    printf("x=%f\n", x); // debug
    if (function(x) > epsilon)
        newton(x, epsilon);
    else
        printf("the zero point (calculated via newton algorithm) lies at f(%f)=%f with a deviation of %f.\n", x, function(x), epsilon);
}